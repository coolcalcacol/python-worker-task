
class Worker:
    def __init__(self, name, hours, pay, total_pay):
        self.name = name
        self.hours = hours
        self.pay = pay
        self.total_pay = total_pay

    def set_name(self, name):
        """
        This function sets our worker instance's name based off the name parameter given.

        :param name: This is used to set our worker instance's name.
        :return:
        """
        self.name = name

    def set_hours(self, hours):
        """
        This function sets the number of hours our worker instance has worked based off the hours parameter given.

        :param hours: This is used to set our worker instance's number of hours worked.
        :return:
        """
        self.hours = hours

    def set_pay(self, pay):
        """
        This function sets the worker instance's pay rate based off the pay parameter given.

        :param pay: This is used to set our worker instance's pay rate.
        :return: Nothing
        """
        self.pay = pay

    def set_total_pay(self):
        """
        This function sets the worker instance's total pay by multiplying their pay by the number of hours they worked

        :return: Nothing
        """
        self.total_pay = self.pay * self.hours



import sys
from tabulate import tabulate
import random
from worker import Worker as createWorker


class Workers:
    """
    This class handles everything to do with employees
    """

    def __init__(self, count):
        """
        This function is run when calling the class, we take in the number of workers
        as a string and assign the number to the class's count variable referenced through
        "self.count"

        :param count: Takes in a String containing the number of workers.
        :raises Exception: Where the input string can't be converted to a number in our range this is raised
        """
        self.average_wage = 0.0
        self.workers = {}
        if count.isdigit() and 1 <= int(count) <= 20:
            self.count = int(count)
        else:
            raise Exception("Invalid input")

    def handle_wages(self):
        """
        this function iterates over each of the workers prompting for a name,
        number of hours worked and pay per hour, creating a new worker class
        for that worker.

        :return: Nothing
        """
        for worker in range(self.count):
            sys.stdout.write("Worker {}/{}".format(worker + 1, self.count))
            name = input("\nWhat is this worker's name?\n")
            try:
                hours = float(input("\rHow many hours did {} work?\n".format(name)))
            except ValueError:
                hours = "NaN"
            try:
                pay = float(input("\rHow much is {} paid per hour?\n".format(name)))
            except ValueError:
                pay = "NaN"
            self.workers[worker] = createWorker(name, hours, pay, "")

    def get_wage_rate(self):
        """
        this function loops over each worker and sets the total pay that worker has earned,
        using the worker class's set_total_pay function.

        :return: Nothing
        """
        for worker in self.workers:
            self.workers[worker].set_total_pay()

    def print_worker_table(self):
        """
        this function outputs a table to console containing each of the workers
        ordered based off the input order of these users.

        :return: Nothing
        """
        table = []
        for worker in self.workers:
            table.append([
                self.workers[worker].name,
                "{:.1f}".format(float(self.workers[worker].hours)),
                "£{:.2f}".format(float(self.workers[worker].pay)),
                "£{:.2f}".format(float(self.workers[worker].total_pay))
            ])
        sys.stdout.write(tabulate(
            table,
            numalign="none",
            floatfmt=("", ".1f", ".2f", ".2f"),
            headers=['Name', 'Hours', 'Pay', 'Total Pay'],
            tablefmt="fancy_grid"
        ))

    def get_average_wage(self):
        """
        This function divides the total pay of all workers by the number of workers,
        to give the average wage of all staff.

        :return: Nothing
        """
        average_wage = 0.0
        for worker in self.workers:
            average_wage += self.workers[worker].pay
        self.average_wage = average_wage / len(self.workers)
        sys.stdout.write("\nAverage wage is £{}".format("{:.2f}".format(self.average_wage)))

    def get_lucky_winner(self):
        """
        This function returns a random member of staff's name as a raffle winner.

        :return: Nothing
        """
        sys.stdout.write("\nLucky winner is: {}".format(random.choice(list(self.workers.values())).name))

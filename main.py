import sys
import time
from workers import Workers

if __name__ == '__main__':
    a = 0
    for x in range(0, 3):
        a = a + 1
        sys.stdout.write("\rLoading" + "." * a)
        time.sleep(1)
    # Add a nice loading bar when the program starts

    workers = Workers(input("\rInput the number of workers\n"))
    # Create an instance of the workers class, referenced to manipulate the
    # workers that the class is passed.

    workers.handle_wages()
    # Takes the number of workers provided when creating the class and creates
    # a worker class for each of them

    workers.get_wage_rate()
    # Takes each worker instance and multiplies their hours of work and pay per
    # hour to give the total they should be paid for the hours they have worked.

    workers.print_worker_table()
    # Outputs a table showing each worker instance's name, hours, pay and total pay

    workers.get_average_wage()
    # Adds up each of the worker instance's pay per hour and divides it by the
    # Number of workers.

    workers.get_lucky_winner()
    # Gets a random worker instance.
